from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def Receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"Receipt_list": receipts}
    return render(request, "receipts/list.html", context)


# @login_required
# def my_receipt_list(request):
#     receipts = Receipt.objects.filter(purchaser=request.user)
#     context = {"Receipt_list": receipts}
#     return render(request, "receipts/list.html", context)


@login_required
def Account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"Account_list": accounts}
    return render(request, "receipts/accountlist.html", context)


@login_required
def ExpenseCategory_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {"Expense_list": expenses}
    return render(request, "receipts/expenselist.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def create_expensecategory(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {"form": form}
    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}
    return render(request, "receipts/createaccount.html", context)
