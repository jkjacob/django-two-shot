from django.urls import path
from receipts.views import (
    Receipt_list,
    create_receipt,
    Account_list,
    ExpenseCategory_list,
    create_expensecategory,
    create_account,
)

urlpatterns = [
    path("", Receipt_list, name="home"),
    path("categories/", ExpenseCategory_list, name="category_list"),
    path("accounts/", Account_list, name="account_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/create/", create_expensecategory, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
